package com.mcm.happyvalentinesday;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.widget.ImageView;

public class MyGLSurfaceView extends GLSurfaceView {
    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private final MyGLRenderer mRenderer;

    public MyGLSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        mRenderer = new MyGLRenderer();
//        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        setRenderer(mRenderer);
    }

    public void setLogo(ImageView logo) {
        mRenderer.setLogo(logo);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:
                float dx = x - mPreviousX;
                mRenderer.setAngle(
                        mRenderer.getAngle() + (dx * TOUCH_SCALE_FACTOR)
                );
                requestRender();
        }
        mPreviousX = x;
        return true;
    }
}
