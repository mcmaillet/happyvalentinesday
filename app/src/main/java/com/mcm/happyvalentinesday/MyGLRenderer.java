package com.mcm.happyvalentinesday;

import android.animation.ValueAnimator;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private float[] mRotationMatrix = new float[16];
    private CardPanel mFrontPanel;
    private CardPanel mBackPanel;
    private ImageView mLogo;
    private boolean isLoaded = false;

    public volatile float mAngle;

    public static int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.89412f, 0.76471f, 0.91765f, 1.0f);
        final float[] frontColor = {1.0f, 0.5f, 0.6f, 1.0f};
        final float[] backColor = {1.0f, 0.2f, 0.3f, 1.0f};
        mFrontPanel = new CardPanel(frontColor);
        mBackPanel = new CardPanel(backColor);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0,
                0, -1.0f, -3.5f,
                -0.05f, 0.25f, 0f,
                0f, 1.0f, 0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        Matrix.setRotateM(mRotationMatrix, 0,
                mAngle,
                0, 1f, 0f);

        // Combine the rotation matrix with the projection and camera view
        // Note that the mMVPMatrix factor *must be first* in order
        // for the matrix multiplication product to be correct.
        float[] scratch = new float[16];
        Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mRotationMatrix, 0);

        mBackPanel.draw(mMVPMatrix);
        mFrontPanel.draw(scratch);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        float ratio = (float) width / height;

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 10);
    }

    public float getAngle() {
        return mAngle;
    }

    public void setAngle(float angle) {
        if (shouldShowLogo(angle)) {
            if(mLogo.getImageAlpha()==0) {
                ValueAnimator animator = ValueAnimator.ofInt(0, 255);
                animator.setDuration(300);
                animator.setInterpolator(new DecelerateInterpolator());
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        mLogo.setImageAlpha((int) valueAnimator.getAnimatedValue());
                    }
                });
                animator.start();
            }
        }else{
            if(mLogo.getImageAlpha()==255){
                ValueAnimator animator = ValueAnimator.ofInt(255, 0);
                animator.setDuration(300);
                animator.setInterpolator(new DecelerateInterpolator());
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        mLogo.setImageAlpha((int) valueAnimator.getAnimatedValue());
                    }
                });
                animator.start();
            }
        }
        mAngle = isInRange(angle) ?
                angle : mAngle;
    }

    private boolean shouldShowLogo(float a) {
        return a <= -90
                && a >= -180;
    }

    private boolean isInRange(float a) {
        return a <= 0
                && a >= -160;
    }

    public void setLogo(ImageView logo) {
        mLogo = logo;
    }
}
